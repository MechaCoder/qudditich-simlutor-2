#!/usr/bin/python3.7
from tinydb import TinyDB, Query, database

class TeamDBexception(BaseException):
    pass

class Team_data:

    def __init__(self, file:str):
        self.file = file
        self.tbl = 'team'

    def _processRows(self, rows:list):
    
        processedList = []
        for row in rows:
            if isinstance(row, database.Document) == False:
                continue

            processedList.append(
                self._processRow(
                    row
                )
            )
        return processedList

    def _processRow(self, row:dict):

        if isinstance(row, database.Document) == False:
            raise TeamDBexception('the passed objecit is not a db row {type(row)}')

        processedDict = {
            'doc_id': row.doc_id,
        }

        for key in row:
            processedDict[key] = row[key]
        
        return processedDict

    def create(self, 
        name:str, keeper:int, seeker:int, 
        chaserOne:int, chaserTwo:int, chaserThree:int,
        beaterOne:int, beaterTwo:int, color1:str, color2:str
    ):

        db = TinyDB(self.file)
        tbl = db.table(self.tbl)

        if tbl.contains(Query().name == name):
            raise TeamDBexception(f'Team name already exists, {name}')

        rawId = tbl.insert({
            'name': name,
            'color1': color1,
            'color2': color2,
            'keeper': keeper,
            'seeker': seeker,
            'chaser1': chaserOne,
            'chaser2': chaserTwo,
            'chaser3': chaserThree,
            'beater1': beaterOne,
            'beater2': beaterTwo
        })

        db.close()
        return rawId

    def getByDocid(self, id:int):

        db = TinyDB(self.file)
        teamTable = db.table(self.tbl)

        rawData = teamTable.get(
            doc_id=id
        )

        db.close()
        return self._processRow(rawData)

    def getByTeamName(self, name:str):

        query = Query()
        db = TinyDB(self.file)
        tbl = db.table(self.tbl)

        rawData = tbl.search(
            query.name == name
        )

        db.close()

        return self._processRows(rows=rawData)