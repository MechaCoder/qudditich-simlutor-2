#!/usr/bin/python3.7
from random import randint
from player.playerObject import Player
from teams.data import Team_data, TeamDBexception

def playerFactory(position:str):
    
    playerA = Player()
    
    while playerA.definePosition() != position:
        playerA = Player()

    
    return playerA

class Team:

    def __init__(self, 
        name:str, 
        color1:str="",
        color2:str="", 
        keeper:Player=playerFactory('Keeper'), 
        seeker:Player=playerFactory('Seeker'), 
        chaser1:Player=playerFactory('Chaser'), 
        chaser2:Player=playerFactory('Chaser'), 
        chaser3:Player=playerFactory('Chaser'), 
        beater1:Player=playerFactory('Chaser'),
        beater2:Player=playerFactory('Chaser')
    ):
        self.name = name

        if len(color1) == 0:
            color1 = self._random_color()

        if len(color2) == 0:
            color2 = self._random_color()

        self.color1 = color1
        self.color2 = color2

        self.keeper = keeper
        self.seeker = seeker
        self.chasers = [
            chaser1,
            chaser2,
            chaser3
        ]

        self.beaters = [
            beater1,
            beater2
        ]

    def _random_color(self):
        randomColor = lambda: randint(0,255)
        return f'#{randomColor()}{randomColor()}{randomColor()}'
