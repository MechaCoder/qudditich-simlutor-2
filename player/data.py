#! /usr/bin/python3.7
from time import time

from tinydb import TinyDB, Query, database

class PlayerDBexception(BaseException):
    pass

class Player_dataBase:

    def __init__(self, fileLocal:str):

        self.file = fileLocal
        self.tbl = 'player'

        self.playerPoition = ['Beater', 'Seeker', 'Keeper', 'Chaser']

    def create(self, name:str,  strength:int, perception:int, endurance:int, intellgence:int, agility:int, luck:int, position:str):
        """ creates the row """

        if position not in self.playerPoition:
            raise PlayerDBexception(f'Invalid position ({position})')

        db = TinyDB(self.file)
        playerTbl = db.table(self.tbl)

        if playerTbl.contains(Query().name == name):
            raise PlayerDBexception(f'Players name already exists, {name}')

        rData = playerTbl.insert({
            'name': name,
            'strength': strength,
            'perception': perception,
            'endurance': endurance,
            'intellgence': intellgence,
            'agility': agility,
            'luck': luck,
            'position': position,
            'timestamp': round(time())
        })

        db.close()
        return rData

    def _processRows(self, rows:list):

        processedList = []
        for row in rows:
            if isinstance(row, database.Document) == False:
                continue

            processedList.append(
                self._processRow(
                    row
                )
            )
        return processedList

    def _processRow(self, row:dict):

        if isinstance(row, database.Document) == False:
            raise PlayerDBexception('the passed objecit is not a db row {type(row)}')

        processedDict = {
            'doc_id': row.doc_id,
        }

        for key in row:
            processedDict[key] = row[key]
        
        return processedDict
            
    def getByPoisition(self, position:str):

        if position not in self.playerPoition:
            raise PlayerDBexception(f'Invalid position ({position})')

        db = TinyDB(self.file)
        playerTbl = db.table(self.tbl)

        rawData = playerTbl.search(
            Query().position == position
        )

        db.close()

        return self._processRows(rows=rawData)

    def getByDocid(self, id:int):

        db = TinyDB(self.file)
        playerTbl = db.table(self.tbl)

        rawData = playerTbl.get(
            doc_id=id
        )

        db.close()

        return self._processRow(rawData)