#! /usr/bin/python3.7
from random import randint

from tinydb import TinyDB, Query

class Player_base:
    """ Player_base class this is to surve as a base class for the players """

    def __init__(self, 
        name:str="", 
        strength:int= -1, 
        perception:int= -1, 
        endurance:int= -1, 
        intellgence:int= -1, 
        agility:int= -1, 
        luck:int= -1
    ):
        if len(name) == 0:
            name = self.genrate_name()

        self.name = name

        if strength == -1:
            strength = self.genrate_trait()
        
        self.strength = strength

        if perception == -1:
            perception = self.genrate_trait()
        
        self.perception = perception

        if endurance == -1:
            endurance = self.genrate_trait()

        self.endurance = endurance

        if intellgence == -1:
            intellgence = self.genrate_trait()

        self.intellgence = intellgence

        if agility == -1:
            agility = self.genrate_trait()

        self.agility = agility

        if luck == -1:
            luck = self.genrate_trait()

        self.luck = luck

    def __str__(self):
        return f"{self.name}| S: {self.strength}, P: {self.perception}, E: {self.endurance}, I: {self.endurance}, A: {self.agility}, L: {self.luck}"

    def genrate_name(self):

        first = [
            'Gordon', 'Bryant', 'Prince', 'Nat', 'Clyde', 
            'Leif', 'Cedar', 'Phoenix', 'Marino', 'Gold', 
            "Millicent", "Susannah", "Mirabelle", "Theodora", 
            "Rosalind", "Zadie", "Aviva", "Devon",
            "Indigo", "Cleo", "Pilar", "Pippa", "Georgiana",
            "Octavia", "Zinnia", "Winifred", "Marguerite",
            "Henrietta", "Greer", "Snow", "Lulu",
            "Tierney", "Verity", "Juno", "Romy",
            "Dinah", "Seren", "Cassia", "Maple",
            "Polly", "Olympia", "Augusta", "Ilaria",
            "Vesper", "Vita", "Nico", "Saskia",
            "Leonora", "Dorothea", "Fay", "Dorian",
            "Mamie", "Eulalia", "Marietta", "Calypso",
            "Flannery", "Indra", "Tamsin", "Bronwen",
            "Fleur", "Lev", "Constantine", "Rio",
            "Shepherd", "Caspian", "Ellison", "Auden",
            "West", "Chester", "Aurelio", "Baxter",
            "Willis", "Baker", "Leopold", "Teo",
            "Stellan", "Roscoe", "Bram", "Cassidy",
            "Penn", "Octavius", "Slater", "True",
            "Nile", "Pax", "Everest", "Rufus",
            "Orson", "Casimir", "Laszlo", "Hawk",
            "Pascal", "Fitzgerald", "Shaw", "Mercer",
            "Ozias", "Bowie", "Larson", "Cosmo",
            "Ellington", "Hart", "Laird", "Griffith",
            "Whit", "Maguire", "Rafferty", "Sacha", "Florian",
            "Peregrine", "Poet", "Scorpus"
        ]
        sir = [
            'Eaglet','Buckthorn','Cerbus','Willows',
            'Spindlewheel','Cerbus','Conifer','Roots',
            'Dogwood',"Annon", "Potter", "Weasly", 
            "Malfoy", "Lovegood", "Belby", "Bones", 
            "Buchanan", "Cattermole", "Chang", "Cooper", "Cram",
            "Crabble", "Crouch", "Delacour", "Diggory", "Dumbledore", 
            "Edgecombe","Flamel", "Flint", "Flume"
        ]

        nick = [
            'Basher',
            'smarty Pants',
            'A-Train',
            'Big Dog',
            'Magic',
            'Rifleman',
            'Stormin Norman',
            'X-Man',
            'Crusher',
            'Jellybean',
            'The Cat',
            'Creepy',
            'Steyn Gun',
            'Wolfie',
            'The Viking',
            'The Bald Eagle'
        ]

        first_name = lambda: first[randint(0, len(first) -1)]
        nickName = lambda: nick[randint(0, len(nick) -1)]
        sir_name = lambda: sir[randint(0, len(sir) -1)]

        rStr = f'{first_name()} {nickName()} {sir_name()}'

        tdb = TinyDB('ds.json')
        t = tdb.table('player')

        loopLimiter = 0

        while t.contains(Query().name == rStr) == True:
            rStr = f'{first_name()} {nickName()} {sir_name()}'

            if loopLimiter >= 10000:
                break
            
            loopLimiter += 1

        tdb.close()
        
        return rStr

    def genrate_trait(self):

        pool = []
        for i in range(1000):
            pool.append(randint(1, 10))

        poolSelect = randint(0, len(pool) - 1)
        return pool[poolSelect]

    def export(self):
        """ exports an dict contain name and speail elements """

        return {
            "name": self.name,
            "strength": self.strength,
            "perception": self.perception,
            "endurance": self.endurance,
            "intellgence": self.intellgence,
            "agility": self.agility,
            "luck": self.luck,
            "position": self.definePosition()
        }
    
    def definePosition(self):

        playersStuff = {
            "strength": self.strength,
            "perception": self.perception,
            "endurance": self.endurance,
            "intellgence": self.intellgence,
            "agility": self.agility
        }

        stongestElement = max(playersStuff, key=playersStuff.get)

        if stongestElement == 'strength':
            return 'Beater'

        if stongestElement == 'perception':
            return 'Seeker'

        if stongestElement == 'endurance':
            return 'Keeper'

        if stongestElement == 'agility':
            return 'Chaser'

        if stongestElement == 'intellgence':
            return 'Chaser'

class Player(Player_base):
    pass

def genrate_name():

    return Player().genrate_name()