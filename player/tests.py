from unittest import TestCase, main
from player import Player_base
# from player.data import Player_dataBase
from player.data import Player_dataBase

class Player_tests(TestCase):

    def test_Player_base_init(self):
        """ tests init and the self grerate """

        testObj = Player_base()

        self.assertIsInstance(
            testObj.name,
            str
        )

        self.assertIsInstance(
            testObj.strength,
            int
        )

        self.assertIsInstance(
            testObj.perception,
            int
        )

        self.assertIsInstance(
            testObj.endurance,
            int
        )

        self.assertIsInstance(
            testObj.intellgence,
            int
        )

        self.assertIsInstance(
            testObj.agility,
            int
        )

        self.assertIsInstance(
            testObj.luck,
            int
        )

    def test_Player_base_genrate_name(self):

        testObj = Player_base().genrate_name()

        self.assertIsInstance(
            testObj,
            str
        )

        brokenDown = testObj.split(' ')
        self.assertEqual(
            len(brokenDown),
            2
        )

    def test_Player_base_genrate_genrate_trait(self):

        testObj = Player_base().genrate_trait()

        self.assertIsInstance(
            testObj,
            int
        )

        self.assertGreater(
            testObj,
            0
        )

        self.assertLess(
            testObj,
            11
        )

    def test_Player_base_export(self):

        testObj = Player_base().export()
        self.assertIn(
            'name',
            testObj.keys()
        )

        self.assertIsInstance(
            testObj['name'],
            str
        )

        self.assertIn(
            'strength',
            testObj.keys()
        )

        self.assertIsInstance(
            testObj['strength'],
            int
        )

        self.assertIn(
            'perception',
            testObj.keys()
        )

        self.assertIsInstance(
            testObj['perception'],
            int
        )

        self.assertIn(
            'endurance',
            testObj.keys()
        )

        self.assertIsInstance(
            testObj['endurance'],
            int
        )

        self.assertIn(
            'intellgence',
            testObj.keys()
        )

        self.assertIsInstance(
            testObj['intellgence'],
            int
        )

        self.assertIsInstance(
            testObj['agility'],
            int
        )

        self.assertIsInstance(
            testObj['luck'],
            int
        )

        self.assertIn(
            'luck',
            testObj.keys()
        )

        self.assertIsInstance(
            testObj['luck'],
            int
        )

        self.assertIn(
            'position',
            testObj.keys()
        )

        self.assertIsInstance(
            testObj['position'],
            str
        )

    def test_Player_base_definePosition(self):

        listEl = ['Beater', 'Seeker', 'Keeper', 'Chaser']

        for e in range(100):
            thisPlayer = Player_base().definePosition()

            self.assertIn(
                thisPlayer,
                listEl
            )
        
class Player_dataBase_tests(TestCase):
    
    def test_Player_dataBase_init(self):

        testObject = Player_dataBase('test.db')

        self.assertIsInstance(
            testObject.file,
            str
        )

        self.assertEqual(
            testObject.file,
            'test.db'
        )

        self.assertIsInstance(
            testObject.tbl,
            str
        )

        self.assertEqual(
            testObject.tbl,
            'player'
        )

    def test_Player_dataBase_create(self):

        testObject = Player_dataBase('test.db')
        
        for i in range(50):
            rowint = testObject.create(
                'test',
                0,
                0,
                0,
                0,
                0,
                0,
                'Seeker'
            )

            self.assertIsInstance(
                rowint,
                int
            )

    def test_Player_dataBase_getByPoisition(self):

        testObj = Player_dataBase('test.db').getByPoisition('Seeker')

        for row in testObj:

            self.assertIsInstance(
                row,
                dict
            )

    def test_Player_dataBase_gegetByDocid(self):

        testObj = Player_dataBase('test.db').getByDocid(2)

        self.assertIsInstance(
            testObj,
            dict
        )




            

        

if __name__ == "__main__":
    main()