#!/usr/bin/python3.7
from random import randint

from player.playerObject import Player_base
from player.data import Player_dataBase, PlayerDBexception


def seedPlayer(fileLocal:str='./db.json', seedRows:int=50):

    # playerObj = Player_base() # gens a player
    data = Player_dataBase(fileLocal)

    for e in range(seedRows):

        playerObj = Player_base().export()

        data.create(
            name=playerObj['name'],
            strength=playerObj['strength'],
            perception=playerObj['perception'],
            endurance=playerObj['endurance'],
            intellgence=playerObj['intellgence'],
            agility=playerObj['agility'],
            luck=playerObj['luck'],
            position=playerObj['position']
        )

    return True


class PlayersMethods:

    def _randomPlayer(self, pool:list):

        return pool[randint(0, len(pool) - 1)]

    def draftTeam(self):

        playerDB = Player_dataBase('./db.json')

        keeperPool = playerDB.getByPoisition('Keeper')
        seekerPool = playerDB.getByPoisition('Seeker')
        chaserPool = playerDB.getByPoisition('Chaser')
        beaterPool = playerDB.getByPoisition('Beater')

        keeper = self._randomPlayer(keeperPool)
        seeker = self._randomPlayer(seekerPool)

        chaser = []

        for i in range(3):
            playerObj = self._randomPlayer(chaserPool)
            while playerObj['doc_id'] in chaser:
                playerObj = self._randomPlayer(chaserPool)
            chaser.append(playerObj['doc_id'])

        beater = []
        for i in range(2):
            playerObj = self._randomPlayer(chaserPool)
            while playerObj['doc_id'] in chaser:
                playerObj = self._randomPlayer(chaserPool)
            beater.append(playerObj['doc_id'])
                

                
        
        beaterOne = self._randomPlayer(beaterPool)
        beaterTwo = self._randomPlayer(beaterPool)

        return {
            "keeper": keeper['doc_id'],
            "seeker": seeker['doc_id'],
            "chasers": chaser,
            "beaters": beater
        }