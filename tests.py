from unittest import main

from player.tests import Player_tests, Player_dataBase_tests

Player_tests()
Player_dataBase_tests()

if __name__ == "__main__":
    main()