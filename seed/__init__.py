#!/usr/bin/python3.7

from player.data import Player_dataBase
from player.playerObject import genrate_name
from teams import Team
from teams.data import Team_data

class Seed:

    def players(self):
        pass

    def teams(self):
        """ Seed Teams with hogwarts """

        teamDataWorker = Team_data('ds.json')
        playerDataWorkder = Player_dataBase('ds.json')

        for e in ['Ravenclaw', 'Hufflepuff', 'Slytherin', 'Griffindor']:
            thisteam = Team(str(e))
            
            keeperInt = playerDataWorkder.create(
                name=genrate_name(),
                strength=thisteam.keeper.strength,
                perception=thisteam.keeper.perception,
                endurance=thisteam.keeper.endurance,
                intellgence=thisteam.keeper.intellgence,
                agility=thisteam.keeper.agility,
                luck=thisteam.keeper.luck,
                position=thisteam.keeper.definePosition()
            )

            seekerInt = playerDataWorkder.create(
                name=genrate_name(),
                strength=thisteam.seeker.strength,
                perception=thisteam.seeker.perception,
                endurance=thisteam.seeker.endurance,
                intellgence=thisteam.seeker.intellgence,
                agility=thisteam.seeker.agility,
                luck=thisteam.seeker.luck,
                position=thisteam.seeker.definePosition()
            )

            chaser1Int = playerDataWorkder.create(
                name=genrate_name(),
                strength=thisteam.chasers[0].strength,
                perception=thisteam.chasers[0].perception,
                endurance=thisteam.chasers[0].endurance,
                intellgence=thisteam.chasers[0].intellgence,
                agility=thisteam.chasers[0].agility,
                luck=thisteam.chasers[0].luck,
                position=thisteam.chasers[0].definePosition()
            )

            chaser2Int = playerDataWorkder.create(
                name=genrate_name(),
                strength=thisteam.chasers[1].strength,
                perception=thisteam.chasers[1].perception,
                endurance=thisteam.chasers[1].endurance,
                intellgence=thisteam.chasers[1].intellgence,
                agility=thisteam.chasers[1].agility,
                luck=thisteam.chasers[1].luck,
                position=thisteam.chasers[1].definePosition()
            )

            chaser3Int = playerDataWorkder.create(
                name=genrate_name(),
                strength=thisteam.chasers[2].strength,
                perception=thisteam.chasers[2].perception,
                endurance=thisteam.chasers[2].endurance,
                intellgence=thisteam.chasers[2].intellgence,
                agility=thisteam.chasers[2].agility,
                luck=thisteam.chasers[2].luck,
                position=thisteam.chasers[2].definePosition()
            )

            beaterOne = playerDataWorkder.create(
                name=genrate_name(),
                strength=thisteam.beaters[0].strength,
                perception=thisteam.beaters[0].perception,
                endurance=thisteam.beaters[0].endurance,
                intellgence=thisteam.beaters[0].intellgence,
                agility=thisteam.beaters[0].agility,
                luck=thisteam.beaters[0].luck,
                position=thisteam.beaters[0].definePosition()
            )

            beaterTwo = playerDataWorkder.create(
                name=genrate_name(),
                strength=thisteam.beaters[1].strength,
                perception=thisteam.beaters[1].perception,
                endurance=thisteam.beaters[1].endurance,
                intellgence=thisteam.beaters[1].intellgence,
                agility=thisteam.beaters[1].agility,
                luck=thisteam.beaters[1].luck,
                position=thisteam.beaters[1].definePosition()
            )

            teamDataWorker.create(
                name=thisteam.name,
                keeper=keeperInt,
                seeker=seekerInt,
                chaserOne=chaser1Int,
                chaserTwo=chaser2Int,
                chaserThree=chaser3Int,
                beaterOne=beaterOne,
                beaterTwo=beaterTwo,
                color1=thisteam.color1,
                color2=thisteam.color2
            )

    def main(self):

        self.players()
        self.teams()