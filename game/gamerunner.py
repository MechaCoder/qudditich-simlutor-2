from random import randint


class GameBase:

    def __init__(self, game):
        """ takes a `GetGameData` that with load two teams from the database."""

        self.home = game.home # the time 
        self.away = game.away

        self.posession = randint(0, 1) 
        #-1 is the game not posession 0 for home and 1 for away
        self.posessionChaser = randint(0, 2)
        # which person has the quffeal 
        

        self.homeScore = 0
        self.awayScore = 0

        self.stop = False

    def _poolMaker(self):

        pool = []
        for obj in dir(self):
            if 'event_' in obj:
                pool.append(obj)
        return pool

    def _getPossession(self):

        if self.posession == 0:
            return self.home
        return self.away

    def _getOpersion(self):

        if self.posession == 1:
            return self.home
        return self.away


class Game(GameBase):
    """ This is the `public` faceing object and takes a get game object.

        nameing Convetions: when adding your own events, you need to follow the
        name convetions always start with `event_` this will define the method as an
        event then define the reguaility event is to happen [`common`, `rare`, `single`].

        single: Single element that should olny happen onece per match an exsample would be captureing 
                the snitch 
    
        rare: these the should be only have a 25% chance of haveing in put into the event pool.

        common: stuff that should be very 
    """

    def event_common_pass(self):
        """ emulates the passing of the Quffal from 
        one player to anouter
        """

        teamObj = self._getPossession()

        currentChaser = teamObj.chasers[self.posessionChaser]

        newPlayer = randint(0, 2)
        while newPlayer == self.posessionChaser:
            newPlayer = randint(0, 2)

        newRandom = teamObj.chasers[newPlayer]

        if currentChaser.agility > randint(0, 10):
            return False

        if newPlayer.agility > randint(0, 10):
            return False

        if (currentChaser.luck + newPlayer.luck) > (randint(0, 10) + randint(0, 10)):
            return False

        # change possesion 
        self.posessionChaser = newPlayer
        return True

    def event_rare_scoreing(self):
        """ this emluteor when someone attempting to score """

        chaser = self._getPossession().chasers[self.posessionChaser]
        keeper = self._getOpersion().keeper

        chaserValue = (chaser.agility + chaser.perception + chaser.luck + randint(0,10))
        keeperValue = (keeper.agility + keeper.endurance + keeper.luck + randint(0,10))

        if chaserValue > keeperValue:
            return False

        if self.posession == 0:
            self.awayScore += 10
            return True
        self.homeScore += 10

    def event_common_tackle(self):
        """ the tackle method emluate """

        chaser = self._getPossession().chasers[self.posession]

        newPoistion = randint(0,2)

        opChaser = self._getOpersion().chasers[newPoistion]
        
        chaserValue = (chaser.agility + chaser.strength + chaser.endurance + randint(0, 10))
        opChaserValue = (opChaser.agility + opChaser.strength + opChaser.endurance + randint(0, 10))

        if opChaserValue > chaserValue:
            
            if self.posession == 0:
                self.posession = 1
            else:
                self.posession = 0
            
            self.posessionChaser = newPoistion
            return True

        return False
