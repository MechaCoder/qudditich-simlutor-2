#! /usr/bin/python3.7

from teams import Team, Team_data, TeamDBexception
from player import Player_base, Player_dataBase, PlayerDBexception
from game.gamerunner import Game

class GameExceptions(BaseException):
    pass

class GetGameData:

    def __init__(self, home:int, away:int):
        """ takes the a team ids one for home and one for array
        this is then made into team objects that can be used to make the game 
        """

        self.home = self._getTeamObject(home)
        self.away = self._getTeamObject(away)

    def _getTeamObject(self, teamId:int):
        """ returns a team object form the db """

        data = {}
        playerDB = Player_dataBase('ds.json')
        
        try:
            data = Team_data('ds.json').getByDocid(teamId)
        except TeamDBexception as err:
            raise GameExceptions(f'the passed id (has not been found)')

        team = Team(name="")

        if 'name' in data.keys():
            team.name = data['name']
        
        if 'color1' in data.keys():
            team.color1 = data['color1']

        if 'color2' in data.keys():
            team.color2 = data['color2']

        if 'keeper' in data.keys():
            playerInfo = playerDB.getByDocid(
                data['keeper']
            )

            team.keeper = Player_base(
                name=playerInfo['name'],
                strength=playerInfo['strength'],
                perception=playerInfo['perception'],
                endurance=playerInfo['endurance'],
                intellgence=playerInfo['intellgence'],
                agility=playerInfo['agility'],
                luck=playerInfo['luck']
            )

        if 'seeker' in data.keys():
            playerInfo = playerDB.getByDocid(
                data['seeker']
            )

            team.seeker = Player_base(
                name=playerInfo['name'],
                strength=playerInfo['strength'],
                perception=playerInfo['perception'],
                endurance=playerInfo['endurance'],
                intellgence=playerInfo['intellgence'],
                agility=playerInfo['agility'],
                luck=playerInfo['luck']
            )

        if 'chaser1' in data.keys():

            playerInfo = playerDB.getByDocid(
                data['chaser1']
            )

            team.chaser1 = Player_base(
                name=playerInfo['name'],
                strength=playerInfo['strength'],
                perception=playerInfo['perception'],
                endurance=playerInfo['endurance'],
                intellgence=playerInfo['intellgence'],
                agility=playerInfo['agility'],
                luck=playerInfo['luck']
            )

        if 'chaser2' in data.keys():
    
            playerInfo = playerDB.getByDocid(
                data['chaser2']
            )

            team.chaser2 = Player_base(
                name=playerInfo['name'],
                strength=playerInfo['strength'],
                perception=playerInfo['perception'],
                endurance=playerInfo['endurance'],
                intellgence=playerInfo['intellgence'],
                agility=playerInfo['agility'],
                luck=playerInfo['luck']
            )

        if 'chaser3' in data.keys():
    
            playerInfo = playerDB.getByDocid(
                data['chaser3']
            )

            team.chaser3 = Player_base(
                name=playerInfo['name'],
                strength=playerInfo['strength'],
                perception=playerInfo['perception'],
                endurance=playerInfo['endurance'],
                intellgence=playerInfo['intellgence'],
                agility=playerInfo['agility'],
                luck=playerInfo['luck']
            )

        if 'beater1' in data.keys():
        
            playerInfo = playerDB.getByDocid(
                data['beater1']
            )

            team.beater1 = Player_base(
                name=playerInfo['name'],
                strength=playerInfo['strength'],
                perception=playerInfo['perception'],
                endurance=playerInfo['endurance'],
                intellgence=playerInfo['intellgence'],
                agility=playerInfo['agility'],
                luck=playerInfo['luck']
            )

        if 'beater2' in data.keys():
            
            playerInfo = playerDB.getByDocid(
                data['beater2']
            )

            team.beater2 = Player_base(
                name=playerInfo['name'],
                strength=playerInfo['strength'],
                perception=playerInfo['perception'],
                endurance=playerInfo['endurance'],
                intellgence=playerInfo['intellgence'],
                agility=playerInfo['agility'],
                luck=playerInfo['luck']
            )

        return team